* Proceeding of Meeting 11
  Date: 13-05
  Started: 11:35
  Ended: 12:24
  Attended: Lauren, Daniel, Isaac

* Mock poster task
  There is a template which can be uploaded into overleaf
  Named Math100_posterl.tex
  Helped daniel with the poster

* Actual Poster
  No one has started working on there section yet
  We are worried week 12 will be a crazy rush to finish the poster

* Will did not show up to the meeting
  Post on forum then contact thalia
  
* Week 11 meeting
  Work on poster?

* Debate on whos name appears on the forum the most
  Isaac vs Lauren
  9 vs 7
  
* Daniel to make forum post about dispute with Will as per contract

* No work to be done by next week
